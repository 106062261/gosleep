function check(start_year, start_month, start_date, start_hour, start_minute, end_year, end_month, end_date, end_hour, end_minute, response) {
    var length = response['length'];
    var i = 0;
    var year, month, date, hour, minute;
    var move = 0;
    var start = 0;
    var end = 0;
    var time = 0;

    start = (start_year * 12) + start_month; //year->month
    start = (start * 30) + start_date; //month->date
    start = (start * 24) + start_hour; //date->hour
    start = (start * 60) + start_minute; //hour->minute

    end = (end_year * 12) + end_month; //year->month
    end = (end * 30) + end_date; //month->date
    end = (end * 24) + end_hour; //date->hour
    end = (end * 60) + end_minute; //hour->minute

    end -= start;

    for (i = 0; i < length; i++) {
        year = (parseInt(response[i]['created_at'][0], 10) * 1000) + (parseInt(response[i]['created_at'][1], 10) * 100) + (parseInt(response[i]['created_at'][2], 10) * 10) + parseInt(response[i]['created_at'][3], 10);
        month = (parseInt(response[i]['created_at'][5], 10) * 10) + parseInt(response[i]['created_at'][6], 10);
        date = (parseInt(response[i]['created_at'][8], 10) * 10) + parseInt(response[i]['created_at'][9], 10);
        hour = (parseInt(response[i]['created_at'][11], 10) * 10) + parseInt(response[i]['created_at'][12], 10);
        minute = (parseInt(response[i]['created_at'][14], 10) * 10) + parseInt(response[i]['created_at'][15], 10);

        time = (year * 12) + month; //year->month
        time = (time * 30) + date; //month->date
        time = (time * 24) + hour; //date->hour
        time = (time * 60) + minute; //hour->minute

        time -= start;

        if (time >= 0 && time <= end) move++;

    }

    return move;
}