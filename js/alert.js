function alert(str,time=2000){
    $(".notify").css("display","block");
    $(".notify").toggleClass("active");
    $("#notifyType").html(str);

    setTimeout(function(){
      $(".notify").removeClass("active");
      setTimeout(()=>{
        $(".notify").css("display","none");
      },400);
    },time);
}