function padLeft(str, len) {
    if (str.length >= len)
        return str;
    else
        return padLeft("0" + str, len);
}
var server_Response = null

function getResponse(y1, mon1, d1, h1, min1, y2, mon2, d2, h2, min2) {
    var AccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU5YzhmZWQ5MzRlNGM0M2Y3NzI1ZDViYjMwYTQwYzJlZGNiZmIzNmE0ODdjODk1YTk1MDY2NGM2YWZhYjQxMjRkNGRhNDUwNDM1M2RhMGNjIn0.eyJhdWQiOiIyIiwianRpIjoiZTljOGZlZDkzNGU0YzQzZjc3MjVkNWJiMzBhNDBjMmVkY2JmYjM2YTQ4N2M4OTVhOTUwNjY0YzZhZmFiNDEyNGQ0ZGE0NTA0MzUzZGEwY2MiLCJpYXQiOjE1NzcwODkxNTYsIm5iZiI6MTU3NzA4OTE1NiwiZXhwIjoxNjA4NzExNTU2LCJzdWIiOiIyOTAiLCJzY29wZXMiOltdfQ.WzBAWA-4_l8f1yx_zestO9YYLIYRTKTK32DNkQJsI_gNUdkc9BLmVEXCkzi2BCgd9KkaUYdUXM3UfrTUTPu70AIxImV6Kvl3o___U4LfmcPSqDdgzRM6aYcNzhYiA81kbZfCMeW_njFzxeL4bMLUiw2oF8mMgzuT59EcW_v75yJOIdVFR5O7YzL-uW9PYXIEyf3CwVebdweEEnA3f4WDyqGgzpOXgvyo4uoL8ihM4pDRFW0YFcn3qu8T8YFOomCEEpc_pCk2eIQmvb5tRmoaD0JCQ_xuox3DPjq3lVSIQiQwOL1SKDhTbDXq_WNvMRws9PYdySFnDRJUtP-4HxUYvQJl38XlLiv0-wX5WnnW3hRntMUivpU8auuuNL2cyvx8LeynhQX7uWag0bLAakEO8B1HZ2nwKW_m1ApcxGRgQJpE3F1ozaQlD_zJOmrey-oUnPIbcjJvXMLOZsXADWNoT23fb8sLoTLvGQJdh2nTAzPCwdqkvQUFJvScRm1S-XPSjMxDx_ecTMFUWMfZcobn7Xa6lSBEgY2otrKqh9qXu0few1bWH0k2m7R-NZkQ6m5TItpwV3EcmfdPrZdzFD6YrHG94XXAr-i6lQJ_VQdlxIZALouc3w4Voj_bSSDDW-ebd_9EVX7GBYub93_gUMo-2YXJ2FhAbPs-k_MjNW9z5Z0";
    var macaddr = "?macaddr=" + "aa386804";
    var date_filter = "&date_filter=" +
        y1 + "-" + padLeft(mon1, 2) + "-" + padLeft(d1, 2) + " " + padLeft(h1, 2) + ":" + padLeft(min1, 2) +
        ":00+-+" +
        y2 + "-" + padLeft(mon2, 2) + "-" + padLeft(d2, 2) + " " + padLeft(h2, 2) + ":" + padLeft(min2, 2) +
        ":00";
    /* var data_array; */
    $.ajax({
        type: "POST",
        url: "https://campus.kits.tw/ICN_API" + macaddr + date_filter,
        dataType: "json",
        async: false,
        success: function (response) {
            server_Response = response
        },
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + AccessToken
        },
        error: function (jqXHR) {
            //alert("Return status: " + jqXHR.status);
            if (jqXHR.status == '200')
                alert("API calling error: macaddr or url format error!");
            else
                alert("API is sleeping !");
        }
    })
}