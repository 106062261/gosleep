jQuery(document).ready(function($) {
    Initial();

    function Initial(){
        firebase.database().ref('schedule').once('value',snapshot=>{
            let data = snapshot.val();
            let n = snapshot.val().number;
            
            cerateScheldule(n, data);
        })
    }

    function cerateScheldule(n, data){
        var schedule = $(".Schcontainer");

        for(var i=1 ; i<=n; i++){
            var id = 'SchBox' + i.toString();
            var title = '';
            var html = '<box class="SchBox" id="' + id + '" onclick="createDetail(' + i +')"  title="'+ title +'"> <t class="Schtext">T' + i.toString() + '</t> </box>'
            
            schedule.append(html);
        }

        var html = '<box class="addbox SchBox"> <t class="addtext">+</t> </box>';

        schedule.append(html);

        $(".addbox").click(()=>{
            popSetting();
        })
    }

    var sleeptime = $(".sleeptime");
    var settingBg = $(".settingBg");
    var settingBox = $(".settingBox");
    var SettingButton = sleeptime.find(".button");

    SettingButton.click(()=>{
        settingBg.css('display','block');
        settingBox.css('display','block');
    })

    function popSetting(){
        settingBg.css('display','block');
        settingBox.css('display','block');
        inputInitial();
    }

    settingBox.find('.button').click(()=>{
        var sinput = $("#sleepInput").val();
        var winput = $("#wakeupInput").val();
        var sdate = sinput.split("/");
        var wdate = winput.split("/");
        var syear = sdate[0].split("-");
        var shour = sdate[1].split(":");
        var wyear = wdate[0].split("-");
        var whour = wdate[1].split(":");

        var sleep = {
            year : parseInt(syear[0]),
            month : parseInt(syear[1]),
            date : parseInt(syear[2]),
            hour : parseInt(shour[0]),
            minute : parseInt(shour[1])
        }

        var wake = {
            year : parseInt(wyear[0]),
            month : parseInt(wyear[1]),
            date : parseInt(wyear[2]),
            hour : parseInt(whour[0]),
            minute : parseInt(whour[1])
        }

        if( sleep.year >1000 & sleep.year <9999 & sleep.month >0 &  sleep.month <13 & sleep.date >0 & sleep.date <32 & sleep.hour >= 0 & sleep.hour <24 & sleep.minute >=0 & sleep.minute <60 &
            wake.year >1000 & wake.year <9999 & wake.month >0 &  wake.month <13 & wake.date >0 & wake.date <32 & wake.hour >= 0 & wake.hour <24 & wake.minute >=0 & wake.minute <60){
                firebase.database().ref('schedule/number').once('value',snapshot=>{
                    var n = snapshot.val() + 1;
                    firebase.database().ref('schedule/number').set(n);
                    firebase.database().ref('schedule/time/' + n + '/sleeptime').set(sleep);
                    firebase.database().ref('schedule/time/' + n + '/wakeuptime').set(wake);

                    var schedule = $(".Schcontainer");
                    var id = 'SchBox' + n.toString();
                    $(".addbox").remove();
                    var html = '<box class="SchBox " id="' + id + '" onclick="createDetail(' + n + ')"> <t class="Schtext">T' + n.toString() + '</t> </box>';
                    schedule.append(html);
                    var html = '<box class="addbox SchBox" > <t class="addtext">+</t> </box>';
                    schedule.append(html);
                    $(".addbox").click(()=>{
                        popSetting();
                    })
                })
                alert("輸入成功");
                closeWindow();
        }
        else{
            alert("輸入格式錯誤");
            closeWindow();
        }
    })

    settingBg.click(()=>{
        closeWindow();
    })

    function inputInitial(){
        var sinput = $("#sleepInput");
        var winput = $("#wakeupInput");

        var date = new Date();
        var h = date.getHours();
        var d = date.getDate();
        var m = date.getMonth()+1;

        if(h > 24 - 8){
            h = 8 - 24 + h;
            d = d + 1;
            if(d > 31){
                d = 1;
                m = m + 1;
            }
        }
        else h = h + 8;

        sinput.val(date.getFullYear() + "-" + ('0' + (date.getMonth()+1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) + "/" + ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2));
        winput.val(date.getFullYear() + "-" + ('0' + m).slice(-2) + "-" + ('0' + d).slice(-2) + "/" + ('0' + h).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2));
    }

    function closeWindow(){
        settingBg.css('display','none');
        settingBox.css('display','none');
        inputInitial();
    }
})
